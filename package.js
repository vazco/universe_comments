'use strict';

Package.describe({
    name: 'vazco:universe-comments',
    summary: 'Comments for Universe',
    version: '0.0.1'
});

Package.onUse(function (api) {

    api.use([
        'templating',
        'underscore',
        'vazco:universe-core',
        'vazco:universe-core-plugin'
    ]);

    api.use([
        'raix:handlebar-helpers',
        'reactive-dict'
    ], 'client');

    api.addFiles([
        'UniComments.js',
        'collection.js',
        'api.js'
    ]);

    api.addFiles([
        'client/api.js',
        'client/scope.js',
        'client/form.html',
        'client/form.js',
        'client/list.html',
        'client/list.js'
    ], 'client');

    api.addFiles([
        'server/publication.js',
        'server/methods.js'
    ], 'server');

    api.export('UniComments');
});