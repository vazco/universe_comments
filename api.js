'use strict';

UniComments.getAllComments = function (containerId) {
    return this.getComments(containerId);
};

UniComments.getLatestComments = function (containerId) {
    return this.getComments(containerId, UniComments.showLatest, true);
};

UniComments.getComments = function (containerId, limit, latest) {
    var options = {};

    if (latest) {
        options.sort = {createdAt: -1};
    } else {
        options.sort = {createdAt: 1};
    }

    if (_.isNumber(limit)) {
        options.limit = limit;
    }

    return this.Comments.find({
        containerId: containerId
    }, options);
};

UniComments.hasMoreComments = function (containerId) {
    var countLatest = UniComments.getLatestComments(containerId).count(),
        countAll = UniComments.getAllComments(containerId).count();
    return countAll > countLatest;
};

UniComments.addComment = function (containerId, text) {
    return this.Comments.insert({
        containerId: containerId,
        text: text,
        createdAt: new Date()
    });
};

UniComments.editComment = function (commentId, text) {
    Meteor.call('uniEditComment', {_id: commentId, text: text})
};