'use strict';

UniComments.addCollection('Comments', {
    onInit: function (collection) {
        collection.Schema = new SimpleSchema({
            containerId: {
                type: String
            },
            text: {
                type: String
            },
            userId: {
                type: String,
                autoValue: function () {
                    if (this.isInsert || this.isUpsert) {
                        return this.userId;
                    }
                },
                denyUpdate: true
            },
            createdAt: {
                type: Date,
                autoValue: function () {
                    if (this.isInsert || this.isUpsert) {
                        return new Date();
                    }
                },
                denyUpdate: true
            }
        });

        collection.attachSchema(collection.Schema);

        collection.helpers({
            getAuthor: function () {
                var collection = UniUsers || Meteor.users;
                if (collection) {
                    return collection.findOne(this.userId);
                }
            }
        });

        collection.allow({
            insert: function () {
                return !!Meteor.userId();
            },
            update: function () {
                return this.userId === Meteor.userId();
            },
            remove: function () {
                return this.userId === Meteor.userId();
            }
        });

        if (Meteor.isServer) {
            collection._ensureIndex({createdAt: -1, containerId:1});
        }
    }
});
