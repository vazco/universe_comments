'use strict';

var showAllComments = new ReactiveDict();

UniComments.addAfterHook('rendered', function () {
    var template = UniComments.getTemplate('uniCommentsList');

    Template[template].destroyed = function () {
        if (this.data && this.data.docId) {
            showAllComments.set(this.data.docId);
        }
    };

    Template[template].rendered = function () {
        this.autorun(function () {
            var data = Template.currentData(),
                docId = data.docId;

            if (docId) {
                if (showAllComments.get(docId)) {
                    UniComments.subscribeAllComments(docId);
                } else {
                    UniComments.subscribeLatestComments(docId);
                }
            }
        });
    };
});

UniComments.addEvents('uniCommentsList', {
    'click .js-show-all': function () {
        if (this.docId) {
            showAllComments.set(this.docId, true);
        }
    }
});

UniComments.addEvents('uniCommentsListItem', {
    'click .js-edit-comment': function (event, template) {
        var text;
        var commentId = Template.instance(2).data._id;
        if (commentId) {
            text = template.$('.comment-text textarea[name="comment-text"]').get(0).value;
            UniComments.editComment(commentId, text);
            Session.set('editCommentId');
        }
    },
    'click .js-delete-comment': function (event) {
        var commentId = Template.instance(2).data._id;
        UniUI.areYouSure(event.currentTarget, function () {
            Meteor.call('uniDeleteComment',commentId);
        });
    },
    'click .js-edit-form': function () {
        var commentId = Template.instance(2).data._id;
        if (commentId) {
            Session.set('editCommentId',commentId);
        }
    }
});

UniComments.addHelpers('uniCommentsList', {
    getComments: function () {
        var docId = this.docId,
            showingAll;

        if (docId) {
            showingAll = showAllComments.get(docId);
            if (showingAll) {
                return UniComments.getAllComments(docId);
            } else {
                return UniComments.getLatestComments(docId)
                    .fetch()
                    .reverse();
            }
        }
    },
    showAllLink: function () {
        var docId = this.docId,
            showingAll,
            hasMore;

        if (docId) {
            showingAll = showAllComments.get(docId);
            hasMore = UniComments.hasMoreComments(docId);

            if (showingAll) {
                return false;
            }
            if (hasMore) {
                return true;
            }
        }
    }
});


UniComments.addHelpers('uniCommentsListItem', {
    isEditMode: function () {
        return Session.get('editCommentId') === Template.instance(2).data._id;
    },
    canEdit: function () {
        return Template.instance(2).data.userId === Meteor.userId();
    }
});
