'use strict';

UniComments.subscribeLatestComments = function (containerId) {
    return Meteor.subscribe(
        'containerComments',
        containerId,
        UniComments.showLatest + 1,
        true
    );
};

UniComments.subscribeAllComments = function (containerId) {
    return Meteor.subscribe(
        'containerComments',
        containerId);
};
