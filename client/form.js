'use strict';

UniComments.sendCommentEvent = function (event, template) {
    var $input = template.find('.js-comment-text'),
        text, docId = this.docId;

    if ($input && docId) {
        text = $input.value;
        if (text) {
            UniComments.addComment(docId, text);
            $input.value = '';
            $($input).trigger('blur');
        }
    }
};

UniComments.addEvents('uniCommentsForm', {
    'click .js-send-comment': UniComments.sendCommentEvent,
    'keypress .js-comment-text': function (event, template) {
        if (event.keyCode === 13) {
            UniComments.sendCommentEvent.call(this, event, template);
        }
    }
});
