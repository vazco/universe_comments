'use strict';

Meteor.methods({
    uniEditComment: function(object){
        check(object._id, String);
        check(object.text, String);
        if(this.userId){
            return UniComments.Comments.update({_id: object._id},{$set:{text: object.text}});
        }
    },
    uniDeleteComment: function(commentId){
        check(commentId, String);
        if(this.userId){
            return UniComments.Comments.remove({_id: commentId});
        }
    }
});