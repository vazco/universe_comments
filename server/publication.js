'use strict';

UniComments.addPublication('containerComments', function (containerId, limit, latest) {
    check(containerId, String);
    check(limit, Match.OneOf(Number, undefined));
    check(latest, Match.OneOf(Boolean, undefined));

    return UniComments.getComments(containerId, limit, latest);
});